package co.fucs.SocketHL7.bussines.imp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;



import org.junit.After;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {co.fucs.SocketHL7.configuration.ConfigurationOnload.class})
public class TestSocketThread
{
	
  @Autowired
  SocketThread sock;
	

  @Before
  public void setUp() 
  {
	  
	  
  }
  
  @After 
  public void tearDown() 
  {
	  
	  
  }
  
  @Test
  public void testThreadTimeLoad() 
  {
	  sock.loadConfiguration();
	  assertTrue(sock.getTimeWait()==100L);
  }
  
  @Test
  public void testThreadBuffSize() 
  {
	  sock.loadConfiguration();
	  assertTrue(sock.getBuffsiz()==10121L);
  }
  
  
  @Test
  public void testThreadPort() 
  {
	  sock.loadConfiguration();
	  assertTrue(sock.getPort()==4600);
  }
  
  @Test
  public void testThreadBackLog() 
  {
	  sock.loadConfiguration();
	  assertTrue(sock.getBaklog()==10);
  }
  
  @Test
  public void testOpenSocket() 
  {
	sock.loadConfiguration();
	//boolean var=sock.openSocketListen();
	//sock.closeSocket();
	assertTrue(true);
  } 
  
  
 
  
  
  
}
