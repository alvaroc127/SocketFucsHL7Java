package co.fucs.SocketHL7.bussines.imp;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.hl7.fhir.instance.model.api.IBase;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.instance.model.api.IIdType;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.BundleType;
import org.hl7.fhir.r4.model.Bundle.HTTPVerb;
import org.hl7.fhir.r4.model.CanonicalType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.DecimalType;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.InstantType;
import org.hl7.fhir.r4.model.MessageHeader;
import org.hl7.fhir.r4.model.MessageHeader.MessageDestinationComponent;
import org.hl7.fhir.r4.model.MessageHeader.MessageSourceComponent;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.SampledData;
import org.hl7.fhir.r4.model.codesystems.BundleTypeEnumFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ca.uhn.fhir.context.FhirContext;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.util.BundleBuilder;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.group.ORU_R01_OBXNTE;
import ca.uhn.hl7v2.model.v231.message.ORU_R01;
import co.fucs.SocketHL7.util.imp.CommonUtil.Constantes;
import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {co.fucs.SocketHL7.configuration.ConfigurationOnload.class})
public class TestHttpJsonRequest 
{
	
	@Autowired
	HttpJsonRequestImp httpreq;
	
	@Autowired
	MapperImp mapp;
	
	@Autowired
	ConfigurationInterfac config;
	
	

	@Before
	public void setUp() throws Exception 
	{
		
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testListElement() 
	{
		String mess="<VT>MSH|^~\\&|^EngineCaptura^|^^|^^|^^|||ORU^R01|0d4835f4a8763bb8-53f4715c-4a981f6-139120e-3c4645e62add0dc1014663c7|P|2.3.1<CR><VT>PID|||PATIENT^196.76.0.4||NO_NAME||20210401153604.324|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR><VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||20210401153604.324<CR><VT>OBR||||^^^^Mindray_Monitor|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG1|125^125^125^126^126^128^129^129^130^130^131^132^132^132^131^130^130^129^129^127^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^121^126^133^137^145^152^159^162^155^148^140^133^126^120^121^122^123^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^126^127^127^129^129^130^131^132^132^133^134^135^135^135^136^136^137^137^137^138^138^138^137^137^136^136^135^135^135^133^133^133^131^130^129^129^127^126^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG2|125^125^125^126^128^130^131^131^132^133^134^135^135^135^134^133^132^131^130^128^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^119^118^127^138^144^155^165^176^180^170^159^148^137^126^118^119^121^122^124^124^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^127^128^128^130^131^132^134^135^136^137^138^139^139^140^141^142^143^143^143^144^144^144^144^143^142^141^140^140^139^137^137^136^134^133^131^130^128^127^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG3|128^128^128^128^129^129^129^130^130^130^130^130^131^130^130^130^130^130^129^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^127^127^128^129^130^131^132^134^134^134^130^125^121^117^113^112^116^120^125^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^129^128^129^129^129^129^128^129^129^129^129^129^129^129^129^129^129^129^129^130^129^129^129^130^130^130^130^130^130^130^130^131^130^130^130^130^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^127^127^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^129^128^128^128^128^129^128^128^128^127^128^128^128^128^128^128^128^128^128^128^128^||||||F@end@<CR>";
		mess=mapp.changeStrings(mess);
		Message msg=mapp.convertMessageToObjt(mess);
		ORU_R01 oru=(ORU_R01)msg;
		java.util.List<ORU_R01_OBXNTE> listO=null;
		try 
		{
		listO=oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBXNTEAll();
		assertTrue(null!=listO);
		}catch (HL7Exception e) 
		{
			System.out.println("se genero un error ");
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testv2TOFhir() 
	{
		String mess="<VT>MSH|^~\\&|^EngineCaptura^|^^|^^|^^|||ORU^R01|0d4835f4a8763bb8-53f4715c-4a981f6-139120e-3c4645e62add0dc1014663c7|P|2.3.1<CR><VT>PID|||PATIENT^196.76.0.4||NO_NAME||20210401153604.324|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR><VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||20210401153604.324<CR><VT>OBR||||^^^^Mindray_Monitor|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG1|125^125^125^126^126^128^129^129^130^130^131^132^132^132^131^130^130^129^129^127^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^121^126^133^137^145^152^159^162^155^148^140^133^126^120^121^122^123^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^126^127^127^129^129^130^131^132^132^133^134^135^135^135^136^136^137^137^137^138^138^138^137^137^136^136^135^135^135^133^133^133^131^130^129^129^127^126^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG2|125^125^125^126^128^130^131^131^132^133^134^135^135^135^134^133^132^131^130^128^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^119^118^127^138^144^155^165^176^180^170^159^148^137^126^118^119^121^122^124^124^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^127^128^128^130^131^132^134^135^136^137^138^139^139^140^141^142^143^143^143^144^144^144^144^143^142^141^140^140^139^137^137^136^134^133^131^130^128^127^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG3|128^128^128^128^129^129^129^130^130^130^130^130^131^130^130^130^130^130^129^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^127^127^128^129^130^131^132^134^134^134^130^125^121^117^113^112^116^120^125^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^129^128^129^129^129^129^128^129^129^129^129^129^129^129^129^129^129^129^129^130^129^129^129^130^130^130^130^130^130^130^130^131^130^130^130^130^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^127^127^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^129^128^128^128^128^129^128^128^128^127^128^128^128^128^128^128^128^128^128^128^128^||||||F@end@<CR>";
		mess=mapp.changeStrings(mess);
		Message msg=mapp.convertMessageToObjt(mess);
		ORU_R01 oru=(ORU_R01)msg;
		java.util.List<ORU_R01_OBXNTE> listO=null;
		FhirContext contex=FhirContext.forR4();
		Bundle bund=new Bundle();
		String arr[]=oru.getMSH().getMsh10_MessageControlID().getValue().split("-");
		bund.setId(new IdType(arr[4]));
		bund.setType(Bundle.BundleType.MESSAGE);
		bund.setTimestampElement(new InstantType(Calendar.getInstance()));
		IParser parse=contex.newJsonParser();
		
		String out=parse.setPrettyPrint(true).encodeResourceToString(bund);
		MessageHeader messagehead=new MessageHeader();
		//System.out.println("mensaje de salida ");
		//System.out.println(out);
	}
	
	
	@Test
	public void testv2TOFhir2() 
	{
		String mess="<VT>MSH|^~\\&|^EngineCaptura^|^^|^^|^^|||ORU^R01|0d4835f4a8763bb8-53f4715c-4a981f6-139120e-3c4645e62add0dc1014663c7|P|2.3.1<CR><VT>PID|||PATIENT^196.76.0.4||NO_NAME||20210401153604.324|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR><VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||20210401153604.324<CR><VT>OBR||||^^^^Mindray_Monitor|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG1|125^125^125^126^126^128^129^129^130^130^131^132^132^132^131^130^130^129^129^127^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^121^126^133^137^145^152^159^162^155^148^140^133^126^120^121^122^123^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^126^127^127^129^129^130^131^132^132^133^134^135^135^135^136^136^137^137^137^138^138^138^137^137^136^136^135^135^135^133^133^133^131^130^129^129^127^126^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG2|125^125^125^126^128^130^131^131^132^133^134^135^135^135^134^133^132^131^130^128^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^119^118^127^138^144^155^165^176^180^170^159^148^137^126^118^119^121^122^124^124^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^127^128^128^130^131^132^134^135^136^137^138^139^139^140^141^142^143^143^143^144^144^144^144^143^142^141^140^140^139^137^137^136^134^133^131^130^128^127^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG3|128^128^128^128^129^129^129^130^130^130^130^130^131^130^130^130^130^130^129^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^127^127^128^129^130^131^132^134^134^134^130^125^121^117^113^112^116^120^125^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^129^128^129^129^129^129^128^129^129^129^129^129^129^129^129^129^129^129^129^130^129^129^129^130^130^130^130^130^130^130^130^131^130^130^130^130^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^127^127^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^129^128^128^128^128^129^128^128^128^127^128^128^128^128^128^128^128^128^128^128^128^||||||F@end@<CR>";
		mess=mapp.changeStrings(mess);
		Message msg=mapp.convertMessageToObjt(mess);
		ORU_R01 oru=(ORU_R01)msg;
		FhirContext contex=FhirContext.forR4();
		Bundle bund=new Bundle();
		String arr[]=oru.getMSH().getMsh10_MessageControlID().getValue().split("-");
		bund.setId(new IdType(arr[4]));
		bund.setType(Bundle.BundleType.MESSAGE);
		bund.setTimestampElement(new InstantType(Calendar.getInstance()));
		//System.out.println("Inicia del entrity");
		MessageHeader messagehead=new MessageHeader();
		messagehead.setId(new IdType(java.util.UUID.randomUUID().toString()));
		//oru.getMSH().getMsh9_MessageType().getMsg2_TriggerEvent().getValue();
		Coding cod =new Coding().setSystem(oru.getMSH().getMsh3_SendingApplication().getHd2_UniversalID().getValue());
		cod.setCode(oru.getMSH().getMsh9_MessageType().getMsg2_TriggerEvent().getValue());
		messagehead.setEvent(cod);
		MessageSourceComponent mes=new MessageSourceComponent();		
		mes.setName(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBR().getObr4_UniversalServiceID().getCe5_AlternateText().getValue());
		mes.setSoftware(cod.getSystem());
		mes.setVersion("1.0");
		messagehead.setSource(mes);
		MessageDestinationComponent destination=new MessageDestinationComponent();
		destination.setName("WEB APP");
		destination.setReceiver(new Reference(new Organization().setId("1")));
		java.util.List<MessageDestinationComponent> list=new java.util.ArrayList<MessageDestinationComponent>();
		list.add(destination);
		messagehead.setDestination(list);
		BundleEntryComponent comp=new BundleEntryComponent();
		comp.setResource(messagehead);
		bund.getEntry().add(comp);
		BundleEntryComponent comp2=new BundleEntryComponent();
		Patient pat=new Patient();
		pat.setId("1");
		
		Address adr=new Address();
		adr.setCity(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid11_PatientAddress()[0].getXad8_OtherGeographicDesignation().getValue());
		pat.addAddress(adr);
		HumanName human=new HumanName();
		human.setFamily(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid5_PatientName()[0].getXpn1_FamilyLastName().getFn1_FamilyName().getValue());
		pat.addName(human);
		HumanName human2=new HumanName();
		human2.setFamily(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid3_PatientIdentifierList()[0].getCx2_CheckDigit().getValue());
		pat.addName(human2);
		comp2.setResource(pat);
		bund.addEntry(comp2);
		
		try {
		java.util.List<ORU_R01_OBXNTE> Obxs=oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBXNTEAll();
		for(ORU_R01_OBXNTE obser:Obxs)
		{
			BundleEntryComponent bund4=new BundleEntryComponent();
			Observation obs=new Observation();
			Coding coder=new Coding();
			coder.setCode(obser.getOBX().getObx3_ObservationIdentifier().getCe1_Identifier().getValue());
			coder.setId(obser.getOBX().getObx3_ObservationIdentifier().getCe2_Text().getValue());
			obs.setCode(new CodeableConcept(coder));
			
			obs.setId(obser.getOBX().getObx4_ObservationSubID().getValue());
			SampledData semp= new SampledData();
			semp.setOrigin(new Quantity().setCode(obser.getOBX().getObx2_ValueType().getValue()));
			String out=obser.getOBX().getObx5_ObservationValue()[0].toString();
			out=out.replace('^',' ');
			semp.setData(out);
			obs.setValue(semp);
			bund4.setResource(obs);
			//System.out.println(obser.getOBX().getObx5_ObservationValue());
			bund.addEntry(bund4);
		}
		}catch (HL7Exception e) 
		{
			e.printStackTrace();
		}
		
		
		IParser parse=contex.newJsonParser();
		
		String out=parse.setPrettyPrint(true).encodeResourceToString(bund);
		
		//System.out.println("mensaje de salida ");
		//System.out.println(out);
	}
	
	
	@Test
	public void testHttp() 
	{
		String mess="<VT>MSH|^~\\&|^EngineCaptura^|^^|^^|^^|||ORU^R01|0d4835f4a8763bb8-53f4715c-4a981f6-139120e-3c4645e62add0dc1014663c7|P|2.3.1<CR><VT>PID|||PATIENT^196.76.0.4||NO_NAME||20210401153604.324|NO_CAPTURE||^|^^^^^^^NO_ADDRES||^^^^^^^^NO_TEL<CR><VT>PV1||O|^||||^^NO_DOCTOR_CAP|||||||||||U||||||||||||||||||||^||||||20210401153604.324<CR><VT>OBR||||^^^^Mindray_Monitor|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**FC demasiado bajo||||||F|||20210401153604.324<CR><VT>OBX||CE|6^ALRM|2|**SpO2 demasiado alto||||||F|||20210401153604.324<CR><VT>OBX||NM|6^ALRM|ALARMAVF|0.060000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVL|0.030000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMAVR|-0.090000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMCVP|0.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGFR|60.000000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGI|0.080000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGII|0.100000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGIII|0.020000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGV|0.040000||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG1|125^125^125^126^126^128^129^129^130^130^131^132^132^132^131^130^130^129^129^127^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^121^126^133^137^145^152^159^162^155^148^140^133^126^120^121^122^123^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^126^127^127^129^129^130^131^132^132^133^134^135^135^135^136^136^137^137^137^138^138^138^137^137^136^136^135^135^135^133^133^133^131^130^129^129^127^126^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG2|125^125^125^126^128^130^131^131^132^133^134^135^135^135^134^133^132^131^130^128^127^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^124^123^122^121^119^118^127^138^144^155^165^176^180^170^159^148^137^126^118^119^121^122^124^124^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^127^128^128^130^131^132^134^135^136^137^138^139^139^140^141^142^143^143^143^144^144^144^144^143^142^141^140^140^139^137^137^136^134^133^131^130^128^127^126^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^125^||||||F<CR><VT>OBX||NM|6^ALRM|ALARMECGECG3|128^128^128^128^129^129^129^130^130^130^130^130^131^130^130^130^130^130^129^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^127^127^128^129^130^131^132^134^134^134^130^125^121^117^113^112^116^120^125^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^129^128^129^129^129^129^128^129^129^129^129^129^129^129^129^129^129^129^129^130^129^129^129^130^130^130^130^130^130^130^130^131^130^130^130^130^129^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^128^128^128^128^129^128^128^128^128^128^128^127^127^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^128^127^127^128^128^128^129^128^128^128^128^129^128^128^128^127^128^128^128^128^128^128^128^128^128^128^128^||||||F@end@<CR>";
		mess=mapp.changeStrings(mess);
		Message msg=mapp.convertMessageToObjt(mess);
		ORU_R01 oru=(ORU_R01)msg;
		FhirContext contex=FhirContext.forR4();
		Bundle bund=new Bundle();
		String arr[]=oru.getMSH().getMsh10_MessageControlID().getValue().split("-");
		bund.setId(new IdType(arr[4]));
		bund.setType(Bundle.BundleType.MESSAGE);
		bund.setTimestampElement(new InstantType(Calendar.getInstance()));
		System.out.println("Inicia del entrity");
		MessageHeader messagehead=new MessageHeader();
		messagehead.setId(new IdType(java.util.UUID.randomUUID().toString()));
		//oru.getMSH().getMsh9_MessageType().getMsg2_TriggerEvent().getValue();
		Coding cod =new Coding().setSystem(oru.getMSH().getMsh3_SendingApplication().getHd2_UniversalID().getValue());
		cod.setCode(oru.getMSH().getMsh9_MessageType().getMsg2_TriggerEvent().getValue());
		messagehead.setEvent(cod);
		MessageSourceComponent mes=new MessageSourceComponent();		
		mes.setName(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBR().getObr4_UniversalServiceID().getCe5_AlternateText().getValue());
		mes.setSoftware(cod.getSystem());
		mes.setVersion("1.0");
		messagehead.setSource(mes);
		MessageDestinationComponent destination=new MessageDestinationComponent();
		destination.setName("WEB APP");
		destination.setReceiver(new Reference(new Organization().setId("1")));
		java.util.List<MessageDestinationComponent> list=new java.util.ArrayList<MessageDestinationComponent>();
		list.add(destination);
		messagehead.setDestination(list);
		BundleEntryComponent comp=new BundleEntryComponent();
		comp.setResource(messagehead).getRequest().setUrl("Bundle").setMethod(HTTPVerb.POST);
		bund.getEntry().add(comp);
		BundleEntryComponent comp2=new BundleEntryComponent();
		Patient pat=new Patient();
		pat.setId("1");
		
		Address adr=new Address();
		adr.setCity(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid11_PatientAddress()[0].getXad8_OtherGeographicDesignation().getValue());
		pat.addAddress(adr);
		HumanName human=new HumanName();
		human.setFamily(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid5_PatientName()[0].getXpn1_FamilyLastName().getFn1_FamilyName().getValue());
		pat.addName(human);
		HumanName human2=new HumanName();
		human2.setFamily(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid3_PatientIdentifierList()[0].getCx2_CheckDigit().getValue());
		pat.addName(human2);
		comp2.setResource(pat);
		bund.addEntry(comp2);
		
		try {
		java.util.List<ORU_R01_OBXNTE> Obxs=oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBXNTEAll();
		for(ORU_R01_OBXNTE obser:Obxs)
		{
			BundleEntryComponent bund4=new BundleEntryComponent();
			Observation obs=new Observation();
			Coding coder=new Coding();
			coder.setCode(obser.getOBX().getObx3_ObservationIdentifier().getCe1_Identifier().getValue());
			coder.setId(obser.getOBX().getObx3_ObservationIdentifier().getCe2_Text().getValue());
			obs.setCode(new CodeableConcept(coder));
			
			obs.setId(obser.getOBX().getObx4_ObservationSubID().getValue());
			SampledData semp= new SampledData();
			semp.setOrigin(new Quantity().setCode(obser.getOBX().getObx2_ValueType().getValue()));
			String out=obser.getOBX().getObx5_ObservationValue()[0].toString();
			out=out.replace('^',' ');
			semp.setData(out);
			obs.setValue(semp);
			bund4.setResource(obs);
			//System.out.println(obser.getOBX().getObx5_ObservationValue());
			bund.addEntry(bund4);
		}
		}catch (HL7Exception e) 
		{
			e.printStackTrace();
		}
		
		
		//IParser parse=contex.newJsonParser();
		
		//String out=parse.setPrettyPrint(true).encodeResourceToString(bund);
		
		//httpreq.sendRequest(bund);
		
		
	}
	

}
