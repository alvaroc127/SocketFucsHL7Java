package co.fucs.SocketHL7.util.imp;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;
import junit.framework.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = {co.fucs.SocketHL7.configuration.ConfigurationOnload.class})
public class TestConfigurationImp {

	@Autowired
	ConfigurationInterfac conf;
	
	@Autowired
	ConfigurationImp confimp;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoadConfiguration() 
	{		
		assertTrue(true);
		//wao que test
	}
	
	@Test
	public void testLoadProp() 
	{
		try {
		confimp.loadProperties("sockthread","D:\\\\fucs\\\\propiedades\\\\socket\\\\socket.properties");
		assertTrue(true);
		}catch (Exception e) {
			e.printStackTrace();
			assertFalse(true);
		}			
	}
	
	@Test
	public void testReadConfiMapp()
	{
		
		try {
		conf.readConfigMapper("D:\\\\fucs\\\\propiedades\\\\mapper\\\\mapper.properties");
		assertTrue(true);
		}catch (Exception e) 
		{
			assertTrue(false);
		}
	}
	
	@Test
	public void testReadConfiSock() {
		ConfigurationImp conf=new ConfigurationImp();
		try {
		conf.readConfigSocket("D:\\\\fucs\\\\propiedades\\\\socket\\\\socket.properties");
		assertTrue(true);
		}catch (Exception e) 
		{
			assertTrue(false);
		}
		
	}
	
	@Test
	public void testReadConfigThread()
	{
		try {
		Properties pro=conf.readConfigWeebHook("D:\\\\fucs\\\\propiedades\\\\webhook\\\\webhook.properties");
		System.out.println(conf.getPropertie("sockthread", "timewait"));
		System.out.println(conf.getPropertie("webhook","ishttps"));
		assertTrue(true);
		}catch (Exception e) 
		{
			assertTrue(false);
		}
		
	}

}
