package co.fucs.SocketHL7.util.imp;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testCommonUtil {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoggerExcepcion() 
	{
		ExcepcionNegocio exp=new 
				ExcepcionNegocio(
				CommonUtil.ERROR.ERROR_GENERAL.getCodigo(),
				CommonUtil.ERROR.ERROR_GENERAL.getError(),
				CommonUtil.ERROR.ERROR_GENERAL.getMensaje());
		CommonUtil.loggerInExcepcion(exp);
		assertTrue(exp.getNum()==CommonUtil.ERROR.ERROR_GENERAL.getCodigo());
	}
	
	@Test
	public void testLoggergetError() 
	{
		ExcepcionNegocio exp=new 
				ExcepcionNegocio(
				CommonUtil.ERROR.ERROR_PROPERT.getCodigo(),
				CommonUtil.ERROR.ERROR_PROPERT.getError(),
				CommonUtil.ERROR.ERROR_PROPERT.getMensaje());
		CommonUtil.loggerInExcepcion(exp);
		assertTrue(exp.getNum()==CommonUtil.ERROR.ERROR_PROPERT.getCodigo());
	}
	
	@Test
	public void testIsNull() 
	{
		assertTrue(true == CommonUtil.isNull(null));
		
	}

}
