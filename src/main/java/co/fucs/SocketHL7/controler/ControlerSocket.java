package co.fucs.SocketHL7.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.uhn.hl7v2.model.v231.message.ORU_R01;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.fhir.r4.model.Bundle;

import co.fucs.SocketHL7.bussines.imp.HttpJsonRequestImp;
import co.fucs.SocketHL7.bussines.imp.MapperImp;
import co.fucs.SocketHL7.bussines.imp.SocketThread;
import co.fucs.SocketHL7.bussines.interfac.HttpJsonRequestInterface;
import co.fucs.SocketHL7.bussines.interfac.MapperInterface;
import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;


@Component
public class ControlerSocket 
{
	
	private static Log log=LogFactory.getLog(ControlerSocket.class);
	

	SocketThread socketThread;
	
	
	ConfigurationInterfac config;
	
	MapperImp map;
	
	HttpJsonRequestInterface httpReq;
	
	
	
	
	public ControlerSocket() 
	{
		super();
	}
	
	
	

	@Autowired(required = true)
	public ControlerSocket(SocketThread socketThread, ConfigurationInterfac configm,MapperImp map,HttpJsonRequestImp httprq) 
	{
		super();
		this.socketThread = socketThread;
		this.config = configm;
		this.map=map;
		this.httpReq=httprq;
	}

	public void startCaptura() 
	{
		log.debug("load properties");
		socketThread.loadConfiguration();
		log.debug("se inicia el hilo de captura");
		if(socketThread.openSocketListen())
		{
			log.debug("seabrio el socket Ok");
			Thread th=new Thread(socketThread);
			th.start();
		}
		getProceesMessage();// inicia el hilo de procesamiento de trama
		log.debug("salida del mensaje");
		//el metodo continua 
	}
	
	
	
	public void getProceesMessage() 
	{
		Thread ht=new Thread(map);
		log.debug("inicio del mapeo");
		map.readQueMessage(socketThread);
		ht.start();
		log.debug("inicio del otro hilo");
		Thread ht2=new Thread(httpReq);
		ht2.start();
	}
	
	
	public void getMessageforHttp() 
	{
		Bundle messageJSON=null;
			if(map.isAvaible()) 
			{
				ORU_R01 mess=map.nexMessage();
				if(null!=mess)messageJSON=httpReq.convertToJsonfhir(mess);
			}
			if(null!=messageJSON) 
			{
				HttpJsonRequestImp httaux=(HttpJsonRequestImp)this.httpReq;
				httaux.addToVector(messageJSON);
			}
	}
	
	
	
	
	

}
