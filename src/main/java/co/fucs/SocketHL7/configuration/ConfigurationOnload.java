package co.fucs.SocketHL7.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"co.fucs.SocketHL7"})
public class ConfigurationOnload {}
