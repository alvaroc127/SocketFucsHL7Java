package co.fucs.SocketHL7.bussines.imp;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Component;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.message.ORU_R01;
import ca.uhn.hl7v2.parser.Parser;
import co.fucs.SocketHL7.bussines.interfac.MapperInterface;
import co.fucs.SocketHL7.util.imp.ConfigurationImp;

import java.util.LinkedList;

import java.util.Queue;
import java.util.concurrent.Semaphore;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


@Component
public class MapperImp implements MapperInterface {

	
	private ConfigurationImp configur;
	
	private SocketThread sock;
	
	private Queue<ORU_R01> messages;
	
	private Semaphore permisThread;
	
	
	
	private Log log=LogFactory.getLog(MapperImp.class);
	
	
	
	@Autowired
	public MapperImp(ConfigurationImp configur) {
		super();
		this.configur = configur;
	}

	public MapperImp() {
		
	}
	
	@Override
	public void run() 
	{
		boolean reader=true;
		boolean outflag=true;
		while(reader) 
		{
			try 
			{
			 if(sock.isAvaible()) 
			 {
				try 
				{
				String mess=sock.getnextMessage(); ///libero el recurso al instante
				if(null != mess && !mess.isEmpty()) 
				{
				mess=changeStrings(mess);
				ORU_R01 oru=(ORU_R01)convertMessageToObjt(mess);
					if(null != oru) 
					{
						log.debug("se paso bien la tuberia");
						do 
					 	{
						outflag=insertMessage(oru); 
					 	}while(!outflag);
					}
				 }
				}catch (Exception e) 
						{
						sock.releses();
						e.printStackTrace();
						log.error("occurrio un error ",e);
						}
			 }
			}catch (Exception e) 
			{
				//sock.releses();
				log.debug("se genero error obtenidneo el semaforo se espera para volve aintentar",e);
			}
		
		}

	}

	@Override
	public void readQueMessage(SocketThread sock) 
	{
		loadConfig();
		this.sock=sock;
	}

	@Override
	public String changeStrings(String msg) 
	{
		msg=msg.replaceAll(MapperInterface.especialVT,"");
		msg=msg.replaceAll(MapperInterface.especitCR,"\r");
		msg=msg.trim();
		return msg;
	}
	
	
	public synchronized boolean isAvaible() 
	{
		return permisThread.tryAcquire();
	}
	
	public void releases() 
	{
		permisThread.release();
	}
	
	
	
	public void loadConfig() 
	{
		int numper=1;
		this.permisThread=new Semaphore(numper);
		messages=new LinkedList<ORU_R01>();
	}
	

	@Override
	public Message convertMessageToObjt(String pipe) 
	{
		HapiContext contex=new DefaultHapiContext();
		Parser parser=contex.getPipeParser();
		try 
		{
			log.debug("voy a convertir el mensaje " + pipe );
			Message msg=parser.parse(pipe);
			
			ORU_R01 oru=(ORU_R01)msg;
			return oru;
		}catch (Exception exp) 
		{
			log.error("error en map",exp);
			exp.printStackTrace();
		}
		
		return null;
	}

	public Queue<ORU_R01> getMessages() {
		return messages;
	}

	public void setMessages(Queue<ORU_R01> messages) {
		this.messages = messages;
	}

	public Semaphore getPermisThread() {
		return permisThread;
	}

	public void setPermisThread(Semaphore permisThread) {
		this.permisThread = permisThread;
	}
	
	
	public boolean insertMessage(ORU_R01 message) 
	{
		boolean output=true;
		if(isAvaible()) 
		{
			this.messages.add(message);
			this.releases();
		}else 
		{
		output=false;		
		}
		return output;
	}
	
	
	public ORU_R01 nexMessage() 
	{
		if(null != this.messages && !this.messages.isEmpty()) 
		{
			ORU_R01 out= this.messages.remove();
			if(!this.isAvaible()) 
			{
			releases();
			}
			return out;
		}else {
			if(!this.isAvaible()) 
			{
			releases();
			}
			return null;
		}
	}
	
	

}
