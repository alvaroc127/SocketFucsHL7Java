package co.fucs.SocketHL7.bussines.imp;

import java.net.SocketAddress;
import java.nio.CharBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;


import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.ByteBuffer;

import co.fucs.SocketHL7.util.imp.CommonUtil.*;
import co.fucs.SocketHL7.util.imp.CommonUtil;

import co.fucs.SocketHL7.bussines.interfac.SocketInterface;
import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;
import java.nio.channels.ServerSocketChannel;
import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * @author alvaro felipe
 * @version 1.0
 * @date 24/02/2020
 * socket para la conexion extiende de thread
 *
 */
@Component
public class SocketThread  implements SocketInterface  
{
	//valor defautl del timewaithread
	
	private static Log log=LogFactory.getLog(SocketThread.class);
	private Long timeWait=1000L;
	private int buffsiz=2024*2;
	private int port=4800;
	private int baklog=5;
	private Semaphore sem;
	private Queue<String> colasMensajes;
	private SocketChannel socketchan;//solo acepta una conexion tcp
	private boolean stop=false;
	
	private ByteBuffer buffer;
	
	private ServerSocketChannel socket;
	
	
	ConfigurationInterfac config;
	
	
	public SocketThread() 
	{
		
	}
	
	@Autowired
	public SocketThread(ConfigurationInterfac configl) 
	{
		this.config=configl;
	}
	


	@Override
	public boolean openSocketListen() 
	{
		log.debug("Iniciando abre socket");
		boolean band=false;
		try 
		{
			SocketAddress sock=new InetSocketAddress(this.port);
			socket=ServerSocketChannel.open();
			socket.bind(sock,this.baklog);	
			log.debug("socket abierto de fomra correcta");
			band=true;
			do {
			socketchan=socket.accept();
			}while(null==socketchan);
		}catch (IOException e) 
		{
			CommonUtil.loggerInExcepcion(e);
		}
		return band;
	}

	@Override
	public void readSocketLoop() 
	{
		StringBuilder strb=new StringBuilder();
		boolean outflag=false;
		while(!stop)
		{
			log.debug("contiuo trabajando");
			try 
			{
			int out=socketchan.read(buffer);
			log.debug("se leen "+out);
			buffer.flip();
			CharBuffer buff=Charset.forName("ISO-8859-1").decode(buffer);
			log.debug("se genera la salida "+buff);
			if(buff.hasArray()) 
				{strb.append(buff.array());}
			else
				{log.debug("No fue posible adjuntar la cadena");}
			buffer=buffer.compact();//se limpiar el buffer leido
			}
			catch (ClosedChannelException ce)
			{
				ce.printStackTrace();
			log.warn("error en "+ce);
			}catch (NotYetConnectedException ne) {
				ne.printStackTrace();
				log.warn("error en "+ne);
			}catch (SecurityException se) {
				se.printStackTrace();
				log.warn("error en "+se);
			}catch (IOException eX) 
			{
				eX.printStackTrace();
			log.warn("error en "+eX);
			}//semaforon para meter en quuque
			if(strb.indexOf("@end@")!=-1) 
			{
				String resul=strb.substring(0, strb.indexOf("@end@")+"@end@".length());
				resul=resul.replaceAll("@end@","");
				do 
				{
				outflag=inserString(resul);
				//reintenta adicionar hasta que tenga permisos para adicionar
				}while(!outflag);
				strb=strb.delete(0, strb.indexOf("@end@")+"@end@".length());
				log.debug("la cadena a procesar xxxx "+ strb.length() +" valor  limpio es vacio "+strb.toString());
			}
			
			
		}
		
	}

	@Override
	public void closeSocket() 
	{
		try {
		this.socket.close();
		}catch (IOException e) 
		{
			log.debug("error abriedn cerrando el hilo");
		}
		
	}

	@Override
	public void addBufferString(String buff) 
	{
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void loadConfiguration() 
	{
		int numper=1;
		String lcport;
		String backString;
		String numPermis;
		String lcTime=config.
		getPropertie(ConfigurationInterfac.PROPERTIES_SOCK,Constantes.THRETIMEWAIT);
		log.debug("valor del objeto ");
		log.debug("valor "+lcTime);
		if(!CommonUtil.isNull(lcTime) && !lcTime.isEmpty()) this.timeWait=Long.valueOf(lcTime);
		String lcBuffSiz=config.
		getPropertie(ConfigurationInterfac.PROPERTIES_SOCK,Constantes.THRESIZEBUFF);
		log.debug("valor del objeto buff ");
		log.debug("valor "+lcBuffSiz);
		if(!CommonUtil.isNull(lcBuffSiz) && !lcBuffSiz.isEmpty()) this.buffsiz=Integer.valueOf(lcBuffSiz);
		lcport=config.
				getPropertie(ConfigurationInterfac.PROPERTIES_SOCK,Constantes.THREPORTCONET);
		log.debug("valor del objeto port ");
		log.debug("valor "+lcport);
		if(!CommonUtil.isNull(lcport) && !lcport.isEmpty()) this.port=Integer.valueOf(lcport);
		backString=config.getPropertie(ConfigurationInterfac.PROPERTIES_SOCK, Constantes.THRECONECT);
		log.debug("valor del objeto port ");
		log.debug("valor "+backString);
		if(!CommonUtil.isNull(backString) && !backString.isEmpty()) this.baklog=Integer.valueOf(backString);
		numPermis=config.getPropertie(ConfigurationInterfac.PROPERTIES_SOCK, Constantes.THREPERMIS);
		log.debug("valor del objeto NUMERO PERMISOS ");
		log.debug("valor "+numPermis);
		if(!CommonUtil.isNull(numPermis) && !numPermis.isEmpty()) numper=Integer.valueOf(numPermis);
		buffer=ByteBuffer.allocate(this.buffsiz);
		colasMensajes=new LinkedList<String>();
		this.sem=new Semaphore(numper);
	}

	public Long getTimeWait() {
		return timeWait;
	}

	public void setTimeWait(Long timeWait) {
		this.timeWait = timeWait;
	}

	public Integer getBuffsiz() {
		return buffsiz;
	}

	public void setBuffsiz(int buffsiz) {
		this.buffsiz = buffsiz;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Semaphore getSem() {
		return sem;
	}

	public void setSem(Semaphore sem) {
		this.sem = sem;
	}

	public int getBaklog() {
		return baklog;
	}

	public void setBaklog(int baklog) {
		this.baklog = baklog;
	}
	
	
	public boolean inserString(String neStr) 
	{
		boolean output=true;
		if(isAvaible()) 
		{
			log.debug("estoy aqui");
			this.colasMensajes.add(neStr);
			this.sem.release();	
		}else
		{
			output=false;
		}
		return output;
	}
	
	public synchronized boolean isAvaible() 
	{
		return sem.tryAcquire();
	}
	
	public void releses() 
	{
		this.sem.release();
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	@Override
	public void run() 
	{
		readSocketLoop();
	}

	@Override
	public String getnextMessage() 
	{	
		if(!this.colasMensajes.isEmpty()) 
		{
		 String out=this.colasMensajes.remove();
		 if(!this.isAvaible()) 
			{
				releses();
			}
		 return out;
		}else {
			if(!this.isAvaible()) 
			{
				releses();
			}
			return null;
		}
	}
	
	
	
	
	
	
}
