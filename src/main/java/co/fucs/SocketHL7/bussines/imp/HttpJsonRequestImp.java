package co.fucs.SocketHL7.bussines.imp;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.InstantType;
import org.hl7.fhir.r4.model.MessageHeader;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Quantity;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.SampledData;
import org.hl7.fhir.r4.model.codesystems.HttpVerb;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Bundle.HTTPVerb;
import org.hl7.fhir.r4.model.MessageHeader.MessageDestinationComponent;
import org.hl7.fhir.r4.model.MessageHeader.MessageSourceComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.valueset.BundleEntrySearchModeEnum;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.SummaryEnum;
import ca.uhn.fhir.rest.client.apache.ApacheHttpClient;
import ca.uhn.fhir.rest.client.apache.ApacheRestfulClientFactory;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.IHttpClient;
import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.impl.BaseClient;
import ca.uhn.fhir.rest.client.impl.GenericClient;
import ca.uhn.fhir.rest.client.interceptor.AdditionalRequestHeadersInterceptor;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v231.group.ORU_R01_OBXNTE;
import ca.uhn.hl7v2.model.v231.message.ORU_R01;
import co.fucs.SocketHL7.bussines.interfac.HttpJsonRequestInterface;
import co.fucs.SocketHL7.util.imp.CommonUtil.Constantes;
import co.fucs.SocketHL7.util.imp.ConfigurationImp;
import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;

@Component
public class HttpJsonRequestImp implements HttpJsonRequestInterface 
{
	
	private ConfigurationInterfac config;
	
	private java.util.Vector<Bundle> jsonsMessage;
	
	private FhirContext contex=FhirContext.forR4();
	
	private static Log log=LogFactory.getLog(HttpJsonRequestImp.class);
	
	
	
	public HttpJsonRequestImp() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public HttpJsonRequestImp(ConfigurationInterfac config) 
	{
		super();
		this.config = config;
		jsonsMessage=new Vector<Bundle>();
	}

	@Override
	public void run() 
	{
		while(true) 
		{
			try 
			{
				if(null != jsonsMessage && !jsonsMessage.isEmpty())
				{
				
				Bundle message=jsonsMessage.remove(0);// falta crear el header y el request
				sendRequest(message);
				}
			}catch (Exception e) 
			{
				log.error("error en el envio del http", e);
			}
		}

	}

	/**
	 * Carga el usuario de alertas, con su contraseña cifrada, o en su defecto su 
	 * OAUTH para poder consumir la api de mensajes. 
	 */
	@Override
	public void loadUser(AdditionalRequestHeadersInterceptor req) 
	{
		log.debug(" se carga el usuario ");
		req.addHeaderValue(Constantes.USER_ALERT,config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.USER_ALERT));
		req.addHeaderValue(Constantes.USER_PASS_CIFRED,config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.USER_PASS_CIFRED));
	}

	@Override
	public void loadTypeRequest() 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void loadBodyRequest() {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendRequest(Bundle json)
	{
		IGenericClient cli;
		
		String ishttps=config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.ISHTTPS);
		log.debug("el valor "+ishttps);
		if(ishttps.equals("true")) 
		{
			cli=createHhttps(json);
		}else {
			cli=createHttp(json);
		}
		config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.SYNCRO);
				
		cli.operation().
		processMessage().setResponseUrlParam
		(config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.RESPONSE_URL))
		.setMessageBundle(json).execute();
		
	}

	/**
	 * este metodo retornar un json, que posterioremente es el body del request
	 */
	@Override
	public Bundle convertToJsonfhir(ORU_R01 oru) 
	{
		
		Bundle bund=new Bundle();
		log.debug("estoy adentro");
		
		String arr[]=oru.getMSH().getMsh10_MessageControlID().getValue().split("-");
		bund.setId(new IdType(arr[4]));
		bund.setType(Bundle.BundleType.MESSAGE);
		String hora=oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPV1PV2().getPV1().getPv144_AdmitDateTime().getTs1_TimeOfAnEvent().getValue();
		SimpleDateFormat simple =new SimpleDateFormat("yyyyMMddHHmmss.SSS");
		String nuevadate=null;
		try 
		{
		java.util.Date out=simple.parse(hora);
		simple.applyPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
		nuevadate=simple.format(out);
		}catch (Exception e) {
			log.debug("error convirtiendo el objeto",e);
		}
		
		bund.setTimestampElement(new InstantType(nuevadate));
		generateHeadMessagBundle(bund, oru);
		generatePatienResourBundle(bund, oru);
		generateObsPatientAlarm(bund, oru);
		IParser parse=contex.newJsonParser();
		log.debug(
		parse.setPrettyPrint(true).encodeResourceToString(bund));
		return bund;
	}

	
	public void generateObsPatientAlarm(Bundle bund, ORU_R01 oru) 
	{
		try {
			java.util.List<ORU_R01_OBXNTE> Obxs=oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBXNTEAll();
			for(ORU_R01_OBXNTE obser:Obxs)
			{
				BundleEntryComponent bund4=new BundleEntryComponent();
				Observation obs=new Observation();
				Coding coder=new Coding();
				coder.setCode(obser.getOBX().getObx3_ObservationIdentifier().getCe1_Identifier().getValue());
				coder.setId(obser.getOBX().getObx3_ObservationIdentifier().getCe2_Text().getValue());
				obs.setCode(new CodeableConcept(coder));
				
				obs.setId(obser.getOBX().getObx4_ObservationSubID().getValue());
				SampledData semp= new SampledData();
				semp.setOrigin(new Quantity().setCode(obser.getOBX().getObx2_ValueType().getValue()));
				String out=obser.getOBX().getObx5_ObservationValue()[0].toString();
				out=out.replace('^',' ');
				semp.setData(out);
				obs.setValue(semp);
				bund4.setResource(obs);
				//System.out.println(obser.getOBX().getObx5_ObservationValue());
				bund.addEntry(bund4);
			}
			}catch (HL7Exception e) 
			{
				e.printStackTrace();
			}
	}
	
	public void generatePatienResourBundle(Bundle bund, ORU_R01 oru) 
	{
		BundleEntryComponent comp2=new BundleEntryComponent();
		Patient pat=new Patient();
		pat.setId(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid3_PatientIdentifierList()[0].getCx2_CheckDigit().getValue());
		Address adr=new Address();
		adr.setCity(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid11_PatientAddress()[0].getXad8_OtherGeographicDesignation().getValue());
		pat.addAddress(adr);
		HumanName human=new HumanName();
		human.setFamily(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid5_PatientName()[0].getXpn1_FamilyLastName().getFn1_FamilyName().getValue());
		pat.addName(human);
		HumanName human2=new HumanName();
		human2.setFamily(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getPIDPD1NK1NTEPV1PV2().getPID().getPid3_PatientIdentifierList()[0].getCx2_CheckDigit().getValue());
		pat.addName(human2);
		comp2.setResource(pat);
		bund.addEntry(comp2);
	}
	
	
	public void generateHeadMessagBundle(Bundle bund, ORU_R01 oru) 
	{
		MessageHeader messagehead=new MessageHeader();
		messagehead.setId(new IdType(java.util.UUID.randomUUID().toString()));
		//oru.getMSH().getMsh9_MessageType().getMsg2_TriggerEvent().getValue();
		Coding cod =new Coding().setSystem(oru.getMSH().getMsh3_SendingApplication().getHd2_UniversalID().getValue());
		cod.setCode(oru.getMSH().getMsh9_MessageType().getMsg2_TriggerEvent().getValue());
		messagehead.setEvent(cod);
		MessageSourceComponent mes=new MessageSourceComponent();		
		mes.setName(oru.getPIDPD1NK1NTEPV1PV2ORCOBRNTEOBXNTECTI().getORCOBRNTEOBXNTECTI().getOBR().getObr4_UniversalServiceID().getCe5_AlternateText().getValue());
		mes.setSoftware(cod.getSystem());
		mes.setVersion("1.0");
		messagehead.setSource(mes);
		MessageDestinationComponent destination=new MessageDestinationComponent();
		destination.setName("WEB APP");
		destination.setReceiver(new Reference(new Organization().setId("1")));
		java.util.List<MessageDestinationComponent> list=new java.util.ArrayList<MessageDestinationComponent>();
		list.add(destination);
		messagehead.setDestination(list);
		BundleEntryComponent comp=new BundleEntryComponent();
		comp.setFullUrl("Bundle").getRequest().setUrl("Bundle").setMethod(HTTPVerb.POST);
		comp.setResource(messagehead);
		bund.getEntry().add(comp);
	}
	
	public java.util.Vector<Bundle> getJsonsMessage() {
		return jsonsMessage;
	}

	public void setJsonsMessage(java.util.Vector<Bundle> jsonsMessage) {
		this.jsonsMessage = jsonsMessage;
	}
	
	public void addToVector(Bundle message) 
	{
		this.jsonsMessage.add(message);	
	}

	@Override
	public IGenericClient createHhttps(Bundle jsonBody) 
	{
		try {
			String oout=
			config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.END_POINT);
			//oout=oout.replaceAll("var1",config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.SYNCRO));
			//oout=oout.replaceAll("var2",config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.RESPONSE_URL));		
			ApacheRestfulClientFactory fac=new ApacheRestfulClientFactory(contex);
			
			
			TrustStrategy var=(cert,authType)->true;
			SSLContext cont=SSLContexts.custom().loadTrustMaterial(null,var).build();
			SSLConnectionSocketFactory sslf=new SSLConnectionSocketFactory(cont, 
		    NoopHostnameVerifier.INSTANCE);
	
			Registry<ConnectionSocketFactory> socketFactoryRegistry = 
				      RegistryBuilder.<ConnectionSocketFactory> create()
				      .register("https", sslf)
				      .register("http", new PlainConnectionSocketFactory())
				      .build();
	
			HttpClientConnectionManager cm= new BasicHttpClientConnectionManager(socketFactoryRegistry);
		    CloseableHttpClient httpClient = HttpClients.custom()
		    		 .setSSLSocketFactory(sslf)
		    	      .setConnectionManager(cm).
		    	      build();
			
			fac.setHttpClient(httpClient);
			IGenericClient igclient= fac.newGenericClient(oout);
			buildRequest(igclient,jsonBody);
			return 	igclient;
		}catch (Exception e) 
		{
			e.printStackTrace();
			return null;
		}
		
	}

	@Override
	public IGenericClient createHttp(Bundle Json) 
	{
		log.debug(" inicia el http client");
		String oout=
				config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.END_POINT);
				//oout=oout.replaceAll("var1",config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.SYNCRO));
				//oout=oout.replaceAll("var2",config.getPropertie(ConfigurationInterfac.PROPERTIES_WEBHOOK,Constantes.RESPONSE_URL));
		IGenericClient gencli=contex.newRestfulGenericClient(oout);
		buildRequest(gencli, Json);
		return gencli;
	}

	
	
	@Override
	public void  buildRequest(IGenericClient httclien,Bundle body) 
	{
		log.debug(" se carga el body del http ");
		buildHead(httclien);		
	}

	@Override
	public void buildHead(IGenericClient gencli) 
	{
		log.debug(" se cargan las propiedades del usuario  ");
		AdditionalRequestHeadersInterceptor adr=new AdditionalRequestHeadersInterceptor();
		loadUser(adr);		
		gencli.registerInterceptor(adr);
	}

	public ConfigurationInterfac getConfig() {
		return config;
	}

	public void setConfig(ConfigurationInterfac config) {
		this.config = config;
	}
	
	
	

}
