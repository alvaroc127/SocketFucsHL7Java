package co.fucs.SocketHL7.bussines.interfac;

import ca.uhn.hl7v2.model.Message;

import co.fucs.SocketHL7.bussines.imp.SocketThread; 


public interface MapperInterface extends Runnable 
{
	public static final String especialVT="<VT>";
	
	public static final String especitCR="<CR>";
			
	
	public void readQueMessage(SocketThread sock);
	
	
	/**
	 * Convierte los caracteres en cadena vacia 
	 * y los <cv> en saltos de linea
	 * @param msg
	 */
	public String changeStrings(final String msg);
	
	
	/**
	 * Para el mensaje hl7 v2.3 en pipe(Sttring) a un objeto para ser transmitod
	 * como JSON, en hub espuesto. el eenpoint espuesto que solo recibe solicitudes 
	 * del usuario cifrado a esa ip por https 
	 * @param pipe
	 * @return
	 */
	public Message convertMessageToObjt(final String pipe);
	
	
	

}
