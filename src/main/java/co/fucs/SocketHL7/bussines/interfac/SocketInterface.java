package co.fucs.SocketHL7.bussines.interfac;

import org.springframework.stereotype.Component;

/**
 * interface para el socket
 * @author Alvaro Coronado
 * @version1.0
 */
@Component
public interface SocketInterface extends Runnable
{
	/**
	 * quedo el socket abierto
	 * @return retornar un boolean con el estado de la apertura
	 */
	public boolean openSocketListen();
	
	/**
	 * lectura del socket
	 */
	public void readSocketLoop();
	
	/**
	 * cierre del socket
	 */
	public void closeSocket();
	
	/**
	 * adiciona  una cadena al buffer compartido
	 * @param buff buffer de entrada
	 */
	public void addBufferString(String buff);
	
	/***
	 * 
	 */
	public void loadConfiguration();
	
	
	public String getnextMessage();
	

}
