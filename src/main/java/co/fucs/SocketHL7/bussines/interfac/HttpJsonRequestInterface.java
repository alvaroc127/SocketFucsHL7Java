package co.fucs.SocketHL7.bussines.interfac;

import org.hl7.fhir.r4.model.Bundle;
import org.springframework.stereotype.Component;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.IHttpRequest;
import ca.uhn.fhir.rest.client.interceptor.AdditionalRequestHeadersInterceptor;
import ca.uhn.hl7v2.model.v231.message.ORU_R01;

@Component
public interface HttpJsonRequestInterface extends Runnable 
{

	/***
	 * 
	 */
	public void loadUser(AdditionalRequestHeadersInterceptor req);
	
	/**
	 * 
	 */
	public void loadTypeRequest() ;
	
	
	/**
	 * 
	 */
	public void loadBodyRequest();
	
	/**
	 * 
	 */
	public void sendRequest(Bundle json);
	
	
	/**
	 * 
	 * @param mess el mensaje
	 * @return json fhir con la conversio
	 */
	public Bundle convertToJsonfhir(ORU_R01 mess);
	
	

	
	
	public IGenericClient createHhttps(Bundle json);
	
	public IGenericClient createHttp(Bundle json);
	
	
	public void  buildRequest(IGenericClient httclien,Bundle body);
	
	
	public void buildHead(IGenericClient gencli);

}
