package co.fucs.SocketHL7.bussines.model;


/*
 * Esta clase se encargara de enviar el mensaje 
 * el dto de mensaje 
 */
public class MessageDTO 
{
	
	String base64;
	
	String nameReq;
	
	String status;

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getNameReq() {
		return nameReq;
	}

	public void setNameReq(String nameReq) {
		this.nameReq = nameReq;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
