package co.fucs.SocketHL7;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import co.fucs.SocketHL7.bussines.imp.SocketThread;
import co.fucs.SocketHL7.configuration.ConfigurationOnload;
import co.fucs.SocketHL7.controler.ControlerSocket;

/**
 * Hello world!
 *
 */
@Component
public class App 
{
	@Autowired
	private ControlerSocket ctrlSocket;
	
	
	static Log log=LogFactory.getLog(App.class);
	
    public static void main( String[] args )
    {
    	log.debug("Inicio del logs por jcl mas xml mas log4j, rapido papa");
    	ApplicationContext cont=new AnnotationConfigApplicationContext(ConfigurationOnload.class);
    	App app=cont.getBean(App.class);
    	app.start();
    	ControlerSocket contsock=cont.getBean(ControlerSocket.class);
    	do {
    		try
    		{
    			log.debug("intento entrar ");
    		 contsock.getMessageforHttp();//encola los mensajes http
    		 Thread.sleep(100);
    		}catch (Throwable e) {
    			e.printStackTrace();
    			log.error("error ",e);
				log.debug("no estop");
			}
    		
        log.debug("Final class");
    	}while(true);
        
    }
    
    public void start() 
    {
    	ctrlSocket.startCaptura();
    }
    
}
