package co.fucs.SocketHL7.util.interfac;

import java.io.FileNotFoundException;
import java.nio.file.InvalidPathException;

import java.util.Properties;

import org.springframework.stereotype.Component;

/**
 * This class load de Configuration Interfac
 * load properties at the HttpRequest
 * load properties at the Mappeer
 * load properties at the Thread
 * @author alvaro coronado 
 * @date 21/02/2021
 *
 */
@Component
public interface ConfigurationInterfac 
{
	
	public static final String FILE_CONFIG_SOCK="properties.propertiesSocketHl7";
	
	public static final String PROPERTIES_SOCK="sockthread";
	
	public static final String PROPERTIES_WEBHOOK="webhook";
	
	public static final String PROPERTIES_MAPP="mapper";
	
	public static final String PROPERTIES_SPLIT_CHAR="@";
	
	
	
	public Properties readConfigSocket(final String key)
	throws java.io.IOException,FileNotFoundException,InvalidPathException,UnsupportedOperationException;
	
	public Properties readConfigMapper(final String key)
	throws java.io.IOException,FileNotFoundException,InvalidPathException,UnsupportedOperationException;
	
	public Properties readConfigWeebHook(final String key)
	throws FileNotFoundException,java.io.IOException,InvalidPathException,UnsupportedOperationException;
	
	
	/**
	 * Este metodo retorna la configuracion para un elemento especifico 
	 * @param Elements is the key word  for Socket Mapper or Thread
	 * @param key the element of the configuration you need get
	 * @return empty string or null or the cofig elements
	 */
	public String getPropertie(String Element,String key);
	

}
