package co.fucs.SocketHL7.util.imp;


public class ExcepcionNegocio extends Exception 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8632202934373911416L;
	private int num;
	private String tip;
	private String messaje;
	
	
	
	public ExcepcionNegocio() 
	{
		super();
	}
	
	
	public ExcepcionNegocio(String ti) 
	{
		super();
		this.tip=ti;
	}
	
	public ExcepcionNegocio(String ti,String messajee) 
	{
		super();
		this.tip=ti;
		this.messaje=messajee;
	}


	public ExcepcionNegocio(int num, String tip, String messaje) {
		super();
		this.num = num;
		this.tip = tip;
		this.messaje = messaje;
	}


	public int getNum() {
		return num;
	}


	public void setNum(int num) {
		this.num = num;
	}


	public String getTip() {
		return tip;
	}


	public void setTip(String tip) {
		this.tip = tip;
	}


	public String getMessaje() {
		return messaje;
	}


	public void setMessaje(String messaje) {
		this.messaje = messaje;
	}
	
	

}
