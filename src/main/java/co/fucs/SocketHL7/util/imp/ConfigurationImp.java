package co.fucs.SocketHL7.util.imp;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;
import java.util.ResourceBundle;
import org.springframework.stereotype.Component;
import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;




/***
 * @author Alvaro Coronado 
 * Implementacion para la carga de propiedades
 */
@Component
public class ConfigurationImp implements ConfigurationInterfac 
{
	private ResourceBundle rs=null;
	
	private HashMap<String,Properties> hasm;
	
	private static Log log=LogFactory.getLog(ConfigurationImp.class);
	
	
	public ConfigurationImp() 
	{
		log.debug("inicia la carga del bundle ");
		this.rs=ResourceBundle.getBundle(FILE_CONFIG_SOCK);
		log.debug(" carga de config ");
		log.debug(FILE_CONFIG_SOCK);
		hasm=new HashMap<String, Properties>();
		String key=null;
		log.debug("Se cargaron las propiedades de inicio ");
		boolean cont=true;
		if(null != rs &&  null != rs.getKeys()) 
		{
			log.debug("entro carga de llaves");
			Enumeration<String> keys=rs.getKeys();
			while(cont && keys.hasMoreElements()) 
			{
				log.debug("moviendo llaves");
				key= keys.nextElement();
				try 
				{
				loadProperties(key,rs.getString(key));
				}catch (Exception e) 
				{
					CommonUtil.loggerInExcepcion(e);
					cont=false;
				}
			}
			
		}		
	}
	
	
	public void loadProperties(final String  key,String value) 
	throws Exception {
		
		log.debug("se valida la llave ");
		log.debug(key);
		
		switch (key) 
		{
		case PROPERTIES_SOCK:
			log.debug("se cargan propieades internas Socket");
			hasm.put(PROPERTIES_SOCK,readConfigSocket(value));
		break;
		
		case PROPERTIES_MAPP:
			log.debug("se cargan propieades internas Mapper");
			hasm.put(PROPERTIES_MAPP,readConfigMapper(value));
		break;
		
		case PROPERTIES_WEBHOOK:
			log.debug("se cargan propieades internas webHook");
			hasm.put(PROPERTIES_WEBHOOK,readConfigWeebHook(value));
		break;

		default:
			log.debug("no se encontraron propieades");
		break;
		}
			
	} 
	

	@Override
	public Properties readConfigSocket(final String key)
	throws java.io.IOException,FileNotFoundException,InvalidPathException,UnsupportedOperationException
	{
		
		log.debug("se cargaran las propiedaes  de la ruta ");
		log.debug(key);
		Path pat=java.nio.file.Paths.get(key);
		log.debug("se cargo la ruta del archivo");
		java.io.File fil=pat.toFile();
		log.debug("se cargo el archivo de forma correcta");
		FileInputStream file=new FileInputStream(fil);
		log.debug("se envian las propiedades y se cargan ");
		Properties prop=new Properties();
		prop.load(file);
		log.debug("propieades cargadas ");	
		return prop;
	}

	@Override
	public Properties readConfigMapper(final String key)
	throws java.io.IOException,FileNotFoundException,InvalidPathException,UnsupportedOperationException
	{
		log.debug("se cargaran las propiedaes  de la ruta ");
		log.debug(key);
		Path pat=java.nio.file.Paths.get(key);
		log.debug("se cargo la ruta del archivo");
		java.io.File fil=pat.toFile();
		log.debug("se cargo el archivo de forma correcta");
		FileInputStream file=new FileInputStream(fil);
		log.debug("se envian las propiedades y se cargan ");
		Properties prop=new Properties();
		prop.load(file);
		log.debug("propieades cargadas ");	
		return prop;
	}

	@Override
	public Properties readConfigWeebHook(final String key)
	throws java.io.IOException,FileNotFoundException,InvalidPathException,UnsupportedOperationException
	{
		log.debug("se cargaran las propiedaes  de la ruta ");
		log.debug(key);
		Path pat=java.nio.file.Paths.get(key);
		log.debug("se cargo la ruta del archivo" +key);
		java.io.File fil=pat.toFile();
		log.debug("se cargo el archivo de forma correcta");
		FileInputStream file=new FileInputStream(fil);
		log.debug("se envian las propiedades y se cargan ");
		Properties prop=new Properties();
		prop.load(file);
		log.debug("propieades cargadas ");
		for(String llava:prop.stringPropertyNames()) 
		{
			log.debug("llaves "+llava);
		}
		
		return prop;
	}


	@Override
	public String getPropertie(String Element, String key) 
	{
		log.debug("Se recibe la busquedad de "+Element);
		Properties pro=this.hasm.get(Element);
		log.debug("valor " + CommonUtil.isNull(pro));
		log.debug("con la propiedad tal "+key);
		if(true == CommonUtil.isNull(pro)) return null;
		return pro.getProperty(key);
	}
	
	
	/*public  String getProperti(final String prop) 
	throws Exception 
	{
		//return this.hasm.get(prop);
	}*/


}
