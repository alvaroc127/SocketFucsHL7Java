package co.fucs.SocketHL7.util.imp;

import java.io.PrintWriter;
import java.io.StringWriter;
import org.apache.commons.logging.LogFactory;

import co.fucs.SocketHL7.util.interfac.ConfigurationInterfac;

import org.apache.commons.logging.Log;

/**
 * 
 * Clase con los metodos para generacion de logs 
 * @author alvaroc127
 *
 */
public  class CommonUtil 
{
	
	private static Log log=LogFactory.getLog(CommonUtil.class);
	
	
	
	public static void loggerInExcepcion(Exception ex) 
	{
		StringWriter sw=new StringWriter();
		PrintWriter pw=new PrintWriter(sw);
		ex.printStackTrace(pw);
		StringBuilder sr=new StringBuilder();
		sr.append("Se genero una excepcion ")
		.append("valide todo la pila para encontrar el error \n")
		.append(sw.toString());
		
		log.trace(sr.toString());
		log.debug(sr.toString());
		log.error(sr.toString());
		log.warn(sr.toString());
		log.info(sr.toString());
	}
	
	
	
	public final class Constantes 
	{
		public static final String THRETIMEWAIT="timewait";
		public static final String THRESIZEBUFF="sizebuff";
		public static final String THRECONECT="maxNumeroConx";
		public static final String THREPORTCONET ="portConnect";
		public static final String THREPERMIS ="numPermis";
		
		public static final String ISHTTPS="ishttps";
		public static final String END_POINT="url_endpoint";
		public static final String SYNCRO="asyn";
		public static final String RESPONSE_URL="response-url";
		public static final String USER_ALERT="userAlertas";
		public static final String USER_PASS_CIFRED="userPasswrod";
				
		
		
		
	}
	
	
	public enum ERROR 
	{
		ERROR_GENERAL("se presento error general ",-99,"ERROR_GEN"),
		ERROR_UTIL("se presento error en un util",-5,"ERROR_UTILI"),
		ERROR_PROPERT("se presento error en los properties",-6,"ERROR_PROPERT");
		
		
		
		private String mensaje;
		private int codigo;
		private String error;
		
		private ERROR(String men, int cod, String err) 
		{
			this.codigo=cod;
			this.mensaje=men;
			this.error=err;			
		}

		public String getMensaje() 
		{
			return mensaje;
		}

		

		public int getCodigo() 
		{
			return codigo;
		}

	

		public String getError() 
		{
			return error;
		}			
	}
	
	public static boolean isNull(Object ob)
	{
		return null == ob?true:false;
	}

}
